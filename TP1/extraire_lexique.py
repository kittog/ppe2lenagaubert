import os
import numpy as np
import pandas as pd

# r1
# path = /path/to/projet/exercice/S1/Corpus
def string_list(filePath):
    """
    construit une liste de chaînes de caractères
    où chaque chaîne correspondra au contenu 
    texte d'un fichier
    """
    l = [] # str list
    f = [] # files list
    # get file name list
    for path in os.listdir(filePath):
        if os.path.isfile(os.path.join(filePath, path)):
            f.append(path)
    
    # open files and increment list
    for file in f:
        with open(filePath + file, 'r') as text:
            l.append(text.read())

    return l


def wordDic(l):
    """
    l : list
    l contient des chaînes de caractères
    wordDic retourne dictionnaire associant chaque mot à
    son nombre d'occurence dans le corpus
    """
    dico = {}
    for i in range(len(l)): # 0, 1
        d = {}
        dump = l[i].strip().split() # liste mots d'un dump textuel
        countWord = pd.value_counts(np.array(dump))
        words = list(countWord.index) # str
        counts = list(countWord.values) # int
        d = dict(zip(words, counts))
        dico.update(d)
    return dico


def wordDicDoc(l):
    """
    l : list
    l contient des chaînes de caractères
    retourne dictionnaire associant chaque mot au nombre de 
    documents dans lequel il apparaît
    """
    dico = {}
    for i in range(len(l)): # 0, 1
        d = {}
        dump = l[i].strip().split() # un dump textuel à la fois
        countWord = pd.value_counts(np.array(dump))
        words = countWord.index # str
        # remplir dictionnaire
        d = dict(zip(words, np.ones(len(words), dtype=int)))
        dico.update(d)
    return dico


